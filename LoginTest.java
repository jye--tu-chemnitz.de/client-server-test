import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.CALLS_REAL_METHODS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.awt.event.ActionEvent;
import java.lang.reflect.Field;

import javax.swing.JTextArea;

import org.junit.BeforeClass;
import org.junit.Test;

public class LoginTest {
	
	Login login = new Login();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		JTextArea jtaUsername = mock(JTextArea.class);
		when(jtaUsername.getText()).thenReturn("admin");
		JTextArea jtaUserPsw = mock(JTextArea.class);
		when(jtaUserPsw.getText()).thenReturn("admin");
		
	}

	@Test
	public void testActionPerformed() throws Exception {
		
		ActionEvent ak = mock(ActionEvent.class);
		when(ak.getSource()).thenAnswer(CALLS_REAL_METHODS);
		//call method
		login.actionPerformed(ak);
		//use reflect to get private
		Class<?> log =login.getClass();
		Field field1 = log.getDeclaredField("username");
		field1.setAccessible(true);
		String string1=(String)field1.get(login);
		
		Field field2 = log.getDeclaredField("userpassword");
		field2.setAccessible(true);
		String string2=(String)field2.get(login);
		//test
		assertEquals(true,login.buttonPressed);
		assertEquals("admin",string1);
		assertEquals("admin",string2);
	}

	@Test
	public void testIsButtonPressed() {
		assertEquals(true,login.buttonPressed);
	}
	
	@Test
	public void testGetUsername() {
		assertEquals("admin",login.jtaUsername);
	}

	@Test
	public void testGetUserpassword() {
		assertEquals("admin",login.jtaUserPsw);
	}

}

package labyrinth.server;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Rule;

import org.mockserver.client.server.MockServerClient;
import org.mockserver.junit.MockServerRule;
import org.mockserver.model.Header;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

import javax.ws.rs.core.MediaType;


public class LabyrinthServerTest {
	
	@Rule
	public MockServerRule server = new MockServerRule(this,8080);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	

	@Test
	public void testGamestatus() throws IOException {
		MockServerClient mockClient = new MockServerClient("localhost", 8080);
		String expected = "You have logged in successfully.";
		
		mockClient.when(
		request()
		.withPath("/Labyrinth.Server/api/gs/game/status")
		.withMethod("POST")
		.withHeader(new Header(HttpHeaders.ACCEPT, MediaType.TEXT_PLAIN))
		.withBody("username=blabla&password=123456")
		).respond(
		response()
		.withStatusCode(200)
		.withBody(expected)
		);
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost("http://localhost:8080/Labyrinth.Server/api/gs/game/status");
		httpPost.addHeader("Accept", "text/plain");
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("username", "blabla"));
		nameValuePairs.add(new BasicNameValuePair("password", "123456"));
		httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		CloseableHttpResponse response = client.execute(httpPost);
		
		InputStream content = response.getEntity().getContent();
		InputStreamReader inputStreamReader = new InputStreamReader(content);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		String responseText = bufferedReader.readLine();
		assertThat(responseText, equalTo(expected));
	}

	

	@Test
	public void testVerifyRESTServer() throws IOException {
		MockServerClient mockClient = new MockServerClient("localhost",8080);
		String expected = "{message:'LabyrinthRESTServer Successfully started!!'}";
		
		mockClient.when(request()
				.withPath("/Labyrinth.Server/api/gs/verify")
				.withMethod("GET"))
				.respond(response().withStatusCode(200)
				.withBody(expected));
		
		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet("http://localhost:8080/Labyrinth.Server/api/gs/verify");
		CloseableHttpResponse response = client.execute(httpGet);
		
		InputStream content = response.getEntity().getContent();
		InputStreamReader inputStreamReader = new InputStreamReader(content);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		String responseText = bufferedReader.readLine();
		assertThat(responseText, equalTo(expected));
	}

}

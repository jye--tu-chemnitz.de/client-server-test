import static org.junit.Assert.*;

import org.junit.Test;

public class FelderTest {
	
	Felder felder = new Felder(false, false, true, true);
	
	@Test
	public void testFelder() {
		assertEquals(false,felder.goUp);
		assertEquals(false,felder.goDown);
		assertEquals(true,felder.goLeft);
		assertEquals(true,felder.goRight);
	}

	@Test
	public void testIsGoUp() {
		assertEquals(false,felder.goUp);
	}

	@Test
	public void testIsGoDown() {
		assertEquals(false,felder.goDown);
	}

	@Test
	public void testIsGoLeft() {
		assertEquals(true,felder.goLeft);
	}

	@Test
	public void testIsGoRight() {
		assertEquals(true,felder.goRight);
	}

	@Test
	public void testMoveClockwise() {
		felder.moveClockwise();
		assertEquals(true,felder.goUp);
		assertEquals(true,felder.goDown);
		assertEquals(false,felder.goLeft);
		assertEquals(false,felder.goRight);
		
	}

	@Test
	public void testMoveConterclockwise() {
		felder.moveClockwise();
		assertEquals(true,felder.goUp);
		assertEquals(true,felder.goDown);
		assertEquals(false,felder.goLeft);
		assertEquals(false,felder.goRight);
	}

}
